//
//  TestApp.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import SwiftUI

@main
struct TestApp: App {
    let dataModel = SourceDataModel()
    var body: some Scene {
        WindowGroup {
            NoticesListView()
        }
    }
}
