//
//  ApliClient.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import Foundation
import UIKit
import SwiftUI



class ApiClient: ObservableObject{
    
    let persistenceData = PersistenceData()
    private var urlRequest: URLRequest?
    private var image: UIImage?
    let constants = Constants()
    func getNotices(param: Bool,  completions: @escaping (Notice?, String?, Int?) -> () = { notices, badUrl, statusCode in}) {
        
        
        
        if param{
            let sour = persistenceData.getSource()
            var queryComponents = URLComponents(string: constants.ENDPOINTURL_WITHPARAMETERS)
            queryComponents?.queryItems = [
                
                URLQueryItem(name: "sources", value: sour)
            ]
             urlRequest = URLRequest(url: (queryComponents?.url)!)
        }else{
            
            guard let url = URL(string: constants.ENDPOINTURL_NOPARAMETER) else {
               return completions(nil, constants.BAD_URL, nil)
            }
            
            urlRequest = URLRequest(url: url)
        }
        let key: String = persistenceData.getApiKey()
        urlRequest!.allHTTPHeaderFields = [ constants.API_KEY_HEADRE: key]
        
        let task = URLSession.shared.dataTask(with: urlRequest!) { [self] data, response, error in
            
            
            if let _ = error{
                
                completions(nil, nil, nil)
            }
            
            if let response = response as? HTTPURLResponse{
                
                print(response.statusCode)
                if response.statusCode == constants.SUCCESSFUL{
                    
                    
                    if let finalData = data{
                        let lolo = String(data: finalData, encoding: .utf8)
                        print(lolo!)
                        do {
                            
                                let notices = try! JSONDecoder().decode(Notice.self, from: finalData)
                                    DispatchQueue.main.async {
                                        completions(notices, nil, constants.SUCCESSFUL)
                                    }
                           
                            
                            
                        } catch  {
                            print(error.localizedDescription)
                            completions(nil, nil, 400)
                        }
                        
                        
                    }
                    
                    
                }else if response.statusCode == constants.API_LIMIT_REQUESTS{
                    
                    completions(nil, nil, constants.API_LIMIT_REQUESTS)
                }
                
            }
      
        }
            completions(nil, nil, nil)
        task.resume()
    }
    
    func getImage(url imageUrl: String) -> UIImage? {
        
        guard let url = URL(string: imageUrl) else {
            return nil
        }
        
        URLSession.shared.dataTask(with: url) { [self] data, response, error in
            
            if let _ = error{
                return
            }
            
            
            if let response = response as? HTTPURLResponse{
                
                if response.statusCode == constants.SUCCESSFUL{
                    
                    if let  imageData = data{
                        let newImage = UIImage(data: imageData)
                        
                            self.image = newImage
                    }
                }
            }

        }.resume()
        return image
    }
    
    
    func saveSource(completion: @escaping (Bool) -> () = {_ in})  {
        
        self.persistenceData.source = constants.SOURCE
        self.persistenceData.saveSource { isSaved in
                completion(isSaved)
            
        }
    }
    
    func saveApiKey() {
        persistenceData.saveApiKey { isSaved in
            if isSaved{
                print("Api key has been saved")
            }
        }
    }
}
