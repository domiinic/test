//
//  Notice.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import Foundation
import UIKit


class Notice: Codable, Identifiable {
    
    var status: String?
    var totalResults: Int?
    var articles: [Articles]?
    var sources: [Source]?
}

class Articles: Codable, Identifiable{

    var source: Source?
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
}

class Source: Codable, Identifiable{
 
    var id: String?
    var name: String?
    var description: String?
    var url: String?
    var category: String?
    var language: String?
    var country: String?
}

struct AllNotice: Codable {
    var status: String?
    var sources: [Source]?
}
