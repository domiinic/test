//
//  PersistenceController.swift
//  Test
//
//  Created by Luis Santana on 2/3/22.
//

import Foundation
import CoreData


struct PersistenceController{
    static let shared = PersistenceController()
    
    let container: NSPersistentContainer
    
    init() {
        container = NSPersistentContainer(name: "ApiKeyDataModel")
        container.loadPersistentStores { description, error in
            
            if let error = error{
                
                fatalError(" error  \(error.localizedDescription)")
            }
            
        }
    }
    
    func save(_ dataModel: NSManagedObjectContext? ,completion: @escaping (Error?) -> () = { _ in}) {
     
            
            do{
                
                try dataModel!.save()
                print("Saved")
                completion(nil)
            }catch{
                completion(error)
            }
        
   
    }
    
    func getApiKey() -> String {
        
        var apiKey = ""
        let apiKeyString: NSFetchRequest<ApiKeyTable> = ApiKeyTable.fetchRequest()
        
        
        do{
            if let key = try self.container.viewContext.fetch(apiKeyString).first?.apiKey{
                apiKey = key
            }
            
        }catch{
            
            return apiKey
        }
        return apiKey
    }
    
    func getSource() -> String{
        
        var source = ""
        let sourceString: NSFetchRequest<SourceDataModel> = SourceDataModel.fetchRequest()
        
        do{
            if let finalSource = try self.container.viewContext.fetch(sourceString).last?.parameter{
                source = finalSource
            }
            
        }catch{
            
            return source
        }
        return source
    }
    
    
}
