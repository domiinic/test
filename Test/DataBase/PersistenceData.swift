//
//  PersistenceData.swift
//  Test
//
//  Created by Luis Santana on 2/3/22.
//

import Foundation
import CoreData
import SwiftUI


class PersistenceData: ObservableObject{
    
    let persistenceData = PersistenceController.shared
    var source: String = ""
    @Published var dataSaved: Bool = false
    let constanst = Constants()
    
    func saveApiKey(completions: @escaping (Bool) -> () = { _ in} ) {
        
            let apiKeyContext = ApiKeyTable(context: persistenceData.container.viewContext)
            apiKeyContext.apiKey = self.constanst.API_KEY
            persistenceData.save(self.persistenceData.container.viewContext) { error in
                if error != nil{
                    DispatchQueue.main.async {
                        self.dataSaved = true
                        completions(self.dataSaved)
                    }
                }
            }
        
    }
    
    func getApiKey() -> String {
        
        return persistenceData.getApiKey()
    }
    
    func saveSource(completions: @escaping (Bool) -> () = { _ in} ) {
        
        let dataContext = SourceDataModel(context: persistenceData.container.viewContext)
        
        
        if !source.isEmpty{
            dataContext.parameter = self.source
            persistenceData.save(self.persistenceData.container.viewContext) { error in
                if error != nil{
                    DispatchQueue.main.async {
                        self.dataSaved = true
                        completions(self.dataSaved)
                    }
                }
            }
        }
    }
    
    func getSource() -> String {
        return persistenceData.getSource()
    }
    
    
}
