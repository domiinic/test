//
//  Constants.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import Foundation

class Constants{
    let API_KEY = "2a2bd21377bd4cb1a3657052986ebc91"
    let API_KEY2 = "45cb67e055fb43eda461a083605203be"
    let API_KEY_HEADRE = "X-Api-Key"
    let SOURCE = "usa-today"
    let ENDPOINTURL_WITHPARAMETERS = "https://newsapi.org/v2/top-headlines?"//sources="
    let ENDPOINTURL_NOPARAMETER = "https://newsapi.org/v2/sources"
    let BAD_URL = "Bad url"
    let SUCCESSFUL = 200
    let noticesScreenName = "Noticias"
    let API_LIMIT_REQUESTS = 429
}
