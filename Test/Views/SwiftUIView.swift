//
//  SwiftUIView.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import SwiftUI

struct SwiftUIView: View {
    var body: some View {
                NavigationView {
                    ZStack {
                        Color.green
                            .opacity(0.6)
                           // .ignoresSafeArea()
                       
                        VStack {
                            Rectangle()
                                .frame(height: 0)
                                .background(Color.green.opacity(0.2))
                            Text("Have the style touching the safe area edge.")
                                .padding()
                            Spacer()
                        }
                        .navigationTitle("Nav Bar Background")
                        .font(.title2)
                    }
                }
            }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
