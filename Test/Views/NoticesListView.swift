//
//  ContentView.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import SwiftUI

struct NoticesListView: View {
    
    @ObservedObject var connectionManager = NetworkManager()
    @ObservedObject var noticesViewModel = NoticeViewModel()
    @State var searchAllNotices: Bool = false
    
    @State var image: UIImage?
    var body: some View {
        GeometryReader{ geo in
            
            if !connectionManager.isConnected{
                VStack{
                    Spacer()
                    HStack{
                        Spacer()
                        VStack{
                            Image(systemName: connectionManager.imageName)
                                .resizable()
                                .frame(width: 50, height: 50)
                                
                            Button {
                                self.noticesViewModel.getNotices(param: true)
                            } label: {
                                Text("Reload")
                                    .frame(width: 70, height: 70).clipShape(Capsule())
                                    .font(Font.system(size: 20))
                            }
                        }
                       Spacer()

                    }
                    Spacer()
                }} else {
                    NavigationView{
                        
                        if noticesViewModel.notices?.articles != nil && connectionManager.isConnected{
                      
                            ArticlesListView(notices: noticesViewModel.notices!, height: geo.size.height, width: geo.size.width, noticesViewModel: noticesViewModel)
                            
                        }else if noticesViewModel.notices?.sources != nil{
                            ArticlesListView(notices: noticesViewModel.notices!, height: geo.size.height, width: geo.size.width, noticesViewModel: noticesViewModel)
                        }
                    }.onAppear {
                        self.noticesViewModel.getNotices(param: true)
                    }
                }
        }
        
    }
    
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        NoticesListView()
    }
}

struct ImageView: View {
    var image: UIImage?
    
    var body: some View{
        
        if image == nil {
         return   Image(systemName: "exclamationmark.triangle")
                .resizable()
                .frame(width: 50, height: 50)
                .cornerRadius(10)
        }else{
           return Image(uiImage: image!)
                .resizable()
                .frame(width: 50, height: 50)
                .cornerRadius(10)
            
        }
        
    }
}


