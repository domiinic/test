//
//  AddSourceView.swift
//  Test
//
//  Created by Luis Santana on 2/3/22.
//

import SwiftUI

struct AddSourceView: View {
    @Environment(\.presentationMode) var presentarionMode
    @StateObject var connectionManager = PersistenceData()
    @State var source: String = ""
    @State var isDataSaved = false
    var body: some View {
            ZStack{
            Color(uiColor: UIColor(named: "Color")!)
                .ignoresSafeArea()
            VStack{
                Text("Save search source").font(Font.system(size: 30))
            TextField(source, text: self.$source)
                .padding()
                .textFieldStyle(RoundedBorderTextFieldStyle())
                
                Button {
                    
                    if !source.isEmpty{
                        self.connectionManager.source = source
                        
                        self.connectionManager.saveSource { isSaved in
                            if isSaved{
                                
                                self.isDataSaved = isSaved
                                //self.presentarionMode.wrappedValue.dismiss()
                        }
                    }
                    presentarionMode.wrappedValue.dismiss()
                    }
                } label: {
                    Text("Save")
                        .padding()
                        .foregroundColor(Color.white)
                        .background(Color.gray)
                        .clipShape(Capsule())
                }
            }.alert(Text("Data Saved"), isPresented: self.$isDataSaved) {
                Button {
                    self.presentarionMode.wrappedValue.dismiss()
                } label: {
                    Text("Data Saved")
                }

            }

        }
    }
}

struct AddSourceView_Previews: PreviewProvider {
    static var previews: some View {
    
        AddSourceView()
    }
}
