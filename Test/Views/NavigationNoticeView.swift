//
//  NavigationView.swift
//  Test
//
//  Created by Luis Santana on 2/3/22.
//

import SwiftUI

struct NavigationNoticeView: View {
    let url: String
    let date: String?
    let title: String
    let width: CGFloat
    let height: CGFloat
    var body: some View {
        NavigationLink {
            NoticeWebView(url: URL(string: url))
        } label: {
            Text(title).lineLimit(nil)
        }.overlay {
            Text("Published At " + convertDate(to:date)).offset(x: width/7, y:height).font(Font.system(size: 12))
        }
    }
    
    func convertDate(to date: String?) -> String {
        
        if date != nil{
            let isoDate = "2016-04-14T10:44:00+0000"

            let dateFormatter = ISO8601DateFormatter()
            let date = dateFormatter.date(from:isoDate)!
            return date.formatted()
        }
       return ""
    }
}

struct NavigationView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationNoticeView(url: "", date: "12-09-0987", title: "Esto es una prueba", width: CGFloat.infinity, height: CGFloat.infinity)
    }
}
