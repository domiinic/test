//
//  ArticlesView.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import SwiftUI

struct ArticlesListView: View {
    let notices: Notice?
    let height: CGFloat
    let width: CGFloat
    var noticesViewModel: NoticeViewModel
    var body: some View {
        
        VStack{
            
            if notices?.sources != nil{
                List(notices!.sources!){ to in
                   
                   HStack{
                       
                       
                           ImageView()
                       
                     
                       NavigationNoticeView(url: to.url!, date: nil, title: to.description!, width: width, height: height/20)
                       
                       
                   }.frame(width: width, height: height/10)
                   
                   
               }.frame(width: width + 35, height:.none)
               .navigationBarTitle("News", displayMode: .inline)
               .navigationBarItems(leading: Button(action: {
                   self.noticesViewModel.getNotices(param: true)
               }, label: {
                   
               Image(systemName: "arrow.clockwise")
                   
               }), trailing: HStack{
                   
                   
                   HStack{
                       Button {
                           self.noticesViewModel.getNotices(param: false)
                       } label: {
                           Text("Load all News")
                       }

                   }
                   
                   NavigationLink(destination: {
                   AddSourceView()
               }, label: {
                   Image(systemName: "plus.app")
               })
                   
               })
            }else{
                List(notices!.articles!){ to in
                   
                   HStack{
                       
                       if to.urlToImage != nil{
                           
                           if let finalImage = noticesViewModel.getImage(url: to.urlToImage!){
                               ImageView(image: finalImage)
                           }
                           
                       }else{
                           ImageView()
                       }
                     
                       NavigationNoticeView(url: to.url!, date: to.publishedAt!, title: to.title!, width: width, height: height/20)
                       
                       
                   }.frame(width: width, height: height/10)
                   
                   
               }.frame(width: width + 35, height:.none)
               .navigationBarTitle("News", displayMode: .inline)
               .navigationBarItems(leading: Button(action: {
                   self.noticesViewModel.getNotices(param: true)
               }, label: {
                   
               Image(systemName: "arrow.clockwise")
                   
               }), trailing: HStack{
                   
                   
                   HStack{
                       Button {
                           self.noticesViewModel.getNotices(param: false)
                       } label: {
                           Text("Load all News")
                       }

                   }
                   
                   NavigationLink(destination: {
                   AddSourceView()
               }, label: {
                   Image(systemName: "plus.app")
               })
                   
               })
            }
             
            
        }
    }
}

struct ArticlesView_Previews: PreviewProvider {
    
    
    static var previews: some View {
        let noti = NoticeViewModel()
        ArticlesListView(notices: Notice(), height: CGFloat.infinity, width: CGFloat.infinity, noticesViewModel: noti)
    }
}
