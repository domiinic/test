//
//  WebView.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import WebKit
import SwiftUI

struct NoticeWebView: UIViewRepresentable {
    
    let url: URL?
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
        
    }
    
    func makeUIView(context: Context) -> some UIView {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        let config = WKWebViewConfiguration()
        config.defaultWebpagePreferences = prefs
        let webView = WKWebView(frame: .zero, configuration: config)
        let requet = URLRequest(url: url!)
        webView.load(requet)
        return webView
    }
     
    
}
