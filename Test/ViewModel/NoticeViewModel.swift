//
//  NoticeViewModel.swift
//  Test
//
//  Created by Luis Santana on 1/3/22.
//

import Foundation
import SwiftUI
import Combine

class NoticeViewModel: ObservableObject {
    @Published var notices: Notice?
    @Published var allNotices: AllNotice?
    @Published var badUrl: String?
    @Published var status: Int = 0000
    @Published var sourceSavingError = ""
    let persistenceData = PersistenceData()
    private let apiClient = ApiClient()
    
    init (){
        self.saveApi()
        self.saveSource()
    }
    
    func getNotices(param: Bool) {
        
        apiClient.getNotices(param: param, completions: { notice, badUrl, statusCode  in
                
            
            DispatchQueue.main.async {
                if let finalNotice = notice{
                    
                        self.notices = finalNotice
                    
                }else if Constants().API_LIMIT_REQUESTS == 429{
                    
                        self.status = Constants().API_LIMIT_REQUESTS
                }else{
                    
                    self.badUrl = Constants().BAD_URL
                }
            }
                
            })
            
    }
    
    func getImage(url: String) -> UIImage? {
       let image = apiClient.getImage(url: url)
        return image
    }
    
    
    func saveApi() {
        let apiKey = persistenceData.getApiKey()
        
        if apiKey == ""{
            self.apiClient.saveApiKey()
        }
        
    }
    
    func saveSource() {
        
        
        let source = persistenceData.getSource()
        if source ==  ""{
            self.apiClient.saveSource { isSaved in
                if !isSaved{
                    self.sourceSavingError = "Source not saved"
                }else{
                    self.getNotices(param: true)
                }
            }
        }else{
            self.getNotices(param: true)
        }
    }
}
