//
//  ConnectionManager.swift
//  Test
//
//  Created by Luis Santana on 2/3/22.
//

import Foundation
import Network

class NetworkManager: ObservableObject{
    
    let monitor = NWPathMonitor()
    let queque = DispatchQueue(label: "NetworkManager")
    @Published var isConnected = true
    
    
    
    var imageName: String{
        return isConnected ?  "wifi" : "wifi.slash"
    }
    
    var connectionDescription: String{
        return isConnected ? "" : "you are not connected to the internet"
    }
    
    init(){
        monitor.pathUpdateHandler = { path in
            print(path.status)
            DispatchQueue.main.async {
                self.isConnected = path.status == .satisfied
            }
        }
        monitor.start(queue: queque)
    }
    
}
